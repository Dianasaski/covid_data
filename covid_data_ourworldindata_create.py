"""
covid_data_ourworldindata_create.py

"""
from sqlalchemy import create_engine
from sqlalchemy import text

from creds import Creds
from covid_data_ourworldindata_settings import Settings
creds = Creds()
settings = Settings()


database_user = creds.database_user
database_pass = creds.database_pass
database_host = settings.database_host
database_name = settings.database_name


def run_database(engine, sql_string):
    """
    """
    connection = engine.connect()
    connection.execute(text(sql_string))
    connection.commit()
    connection.close()


def run_create_database(database_name):
    """
    """
    print(f"running create database {database_name}")
    run_database(
        create_engine(
            f"postgresql://{database_user}:{database_pass}@{database_host}",
            isolation_level='AUTOCOMMIT',
        ),
        f"create database {database_name}",
    )


def run_create_tables(sql_string):
    """
    """
    run_database(
        create_engine(
            f"postgresql://{database_user}"
            f":{database_pass}@{database_host}"
            f"/{database_name}"
        ),
        sql_string,
    )


def create(covid_data):

    # to delete run_create_database(database_name)
    print("database created")

    print("getting all columns for creating country table")
    string_meta_cols = str()
    for c in covid_data.columns:
        if c not in ("continent", "data"):
            # TODO: determine type
            string_meta_cols += f"{c} text,\n"

    print("getting all keys for creating country_data table")
    all_unique_data_keys = set()
    for country_datas in covid_data["data"]:
        for country_data in country_datas:
            all_unique_data_keys.update(
                [k for k in country_data.keys()]
            )
    all_unique_data_keys = sorted(all_unique_data_keys)

    cd_cols = str()
    for k in all_unique_data_keys:
        # TODO: determine type
        cd_cols += f"    {k} text,\n"

    table_creation_sql = f"""
create table if not exists continents(
    continent_id serial primary key,
    continent text
);

create table if not exists countries(
    country_id serial primary key,
    country text,
    {string_meta_cols}
    continent_id int,
    foreign key (continent_id)
        references continents(continent_id)
);

create table if not exists country_data(
    country_data_id serial primary key,
    {cd_cols}
    country_id int,
    foreign key (country_id)
        references countries(country_id)
        on delete cascade
);
    """

    print("sending sql for creating all tables")
    run_create_tables(table_creation_sql)
    print("table creation sql:")
    print(table_creation_sql)
    print("done creating tables")
