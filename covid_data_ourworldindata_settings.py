from dataclasses import dataclass


@dataclass
class Settings:
    database_host = "localhost"
    database_name = "test_one"
