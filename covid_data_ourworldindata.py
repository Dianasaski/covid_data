"""
covid_data_ourworldindata.py

"""
from covid_data_ourworldindata_retrieve import retrieve
from covid_data_ourworldindata_create import create
from covid_data_ourworldindata_insert import insert


def covid_data_ourworldindata():
    """run all the parts in order
    """
    print("retrieving data")
    covid_data = retrieve()
    print("data retrieved")

    print("creating database")
    create(covid_data)
    print("database created")

    print("inserting data")
    insert(covid_data)
    print("data inserted")

    print("done")


if __name__ == "__main__":
    covid_data_ourworldindata()
