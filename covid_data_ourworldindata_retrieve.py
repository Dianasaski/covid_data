"""
covid_data_ourworldindata_retrieve.py

"""
import pandas


def retrieve():
    """
    """
    url = (
        "https://covid.ourworldindata.org"
        "/data/owid-covid-data.json"
    )
    print(f"fetching data from {url}")
    data = pandas.read_json(url)
    print("data fetched, creating data frame")
    df = pandas.DataFrame(data, columns=data.keys())
    print("transposing data frame and returning")
    return df.T
